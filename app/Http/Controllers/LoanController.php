<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Validator;
use App\Models\Loan;

class LoanController extends Controller
{
    public function loan(Request $request){

        $validated = [
            'amount' => 'required|numeric',
            'arrangementFee' => 'required|numeric|max:100',
            'interestRate' => 'required|numeric|max:100',
            'repaymentFrequency' => 'required|max:255',
            'duration' => 'required|numeric'
        ];

        $validator = Validator::make($request->all(), $validated);

        if($validator->fails()){
            return response()->json([
                "status" => 'Error',
                "message" => $validator->errors(),
            ], 422);
        }

        $loan = new Loan();
        $loan->user_id = JWTAuth::user()->id;
        $loan->amount = $request->amount;
        $loan->arrangement_fee = $request->arrangementFee;
        $loan->interest_rate = $request->interestRate;
        $loan->repayment_frequency = $request->repaymentFrequency;
        $loan->duration = $request->duration;
        $loan->save();

        return response()->json(['status' => 'Success', 'loan' => $loan]);
    }
}
