<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Http\Resources\UserResource;

class UserController extends Controller
{
    public function register(Request $request){

        $validated = [
            'name' => 'required|max:255',
            'email' => 'required|unique:users|email|max:255',
            'password' => 'required|min:6',
        ];

        $validator = Validator::make($request->all(), $validated);

        if($validator->fails()){
            return response()->json([
                "error" => 'Missing field',
                "message" => $validator->errors(),
            ], 422);
        }

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->save();

        try {
            if (! $token = JWTAuth::attempt( $validator->validated() )) {
                return response()->json(['error' => 'Unauthorized'], 401);
            }
        } catch (JWTAuthException $e) {
            return response()->json(['failed_to_create_token'], 500);
        }

        $userLogin = JWTAuth::user();
        $userLogin['token'] = $token;

        return new UserResource($userLogin);
    }
}
