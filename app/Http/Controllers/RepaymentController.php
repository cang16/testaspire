<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Validator;
use App\Models\Repayment;
use App\Models\Loan;


class RepaymentController extends Controller
{
    public function repayment(Request $request){

        $validated = [
            'loanId' => 'required',
            'amount' => 'required|numeric',
        ];

        $validator = Validator::make($request->all(), $validated);

        if($validator->fails()){
            return response()->json([
                "status" => 'Error',
                "message" => $validator->errors(),
            ], 422);
        }

        $loanId = $request->loanId;
        $loan = Loan::find($loanId);
        if(!$loan){
            return response()->json(['status' => 'Error', 'message' => 'Loan not found']);
        }
        $totalAmountLoan = $loan->amount + (($loan->amount * $loan->interest_rate) / 100) + (($loan->amount * $loan->arrangement_fee) / 100);

        $getRepayment = Repayment::where('loan_id', $loanId);
        $countRepayment = (clone $getRepayment)->count();
        $sumAmount = (clone $getRepayment)->sum('amount');
        if($sumAmount >= $totalAmountLoan){
            return response()->json(['status' => 'Error', 'message' => 'This loan has been full repaid']);
        }

        $repayment = new Repayment();
        $repayment->loan_id = $request->loanId;
        $repayment->amount = $request->amount;
        $repayment->times = $countRepayment + 1;
        $repayment->date = isset($request->date) ? date('Y-m-d', strtotime($request->date)) : date('Y-m-d');
        $repayment->save();

        return response()->json(['status' => 'Success', 'repayment' => $repayment]);
    }
}
