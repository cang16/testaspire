<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

# Getting started

## Installation

Please check the official laravel installation guide for server requirements before you start. [Official Documentation](https://laravel.com/docs/5.4/installation#installation)

Clone the repository

    git clone https://gitlab.com/cang16/testaspire.git

Switch to the repo folder

    cd testaspire

Install all the dependencies using composer

    composer install

Copy the example env file and make the required configuration changes in the .env file

    cp .env.example .env

Generate a new application key

    php artisan key:generate

Generate a new JWT authentication secret key

    php artisan jwt:secret

## Installation Mysql

### Windows

Install [XAMPP](https://www.apachefriends.org/index.html) to setup Mysql for Windows

    1/ Download Xampp application
    2/ Open Xampp control panel
    3/ Start Apache and Mysql
    4/ Access http://localhost/phpmyadmin
    5/ CREATE DATABASE testaspire

Beside that you can use [Homestead](https://laravel.com/docs/8.x/homestead#introduction) to setup server and mysql

    1/ cd Homestead
    2/ Open Homestead.yaml
    3/ Config folders contain code after clone form gitlab and site
    4/ Add `- testaspire` below `databases:` to create new database
    5/ Open Notepad with Run as administator
    6/ Open file hosts in path C:\Windows\System32\drivers\etc
    7/ Add new domain same domain in file Homestead.yaml. Example: 192.168.10.10 testaspire.test
    8/ Save file hosts
    9/ Open Powershell or Git in folder Homestead. Run `vagrant up` or `vagrant reload --provision` to start server
    10/ Access http://testaspire.test

Config file .env inside project if using Homestead

    APP_URL=http://testaspire.test

    DB_HOST=192.168.10.10
    DB_PORT=3306
    DB_DATABASE=testaspire
    DB_USERNAME=homestead
    DB_PASSWORD=secret

### MacOS

If you use MacOS, you can use [Valet](https://laravel.com/docs/8.x/valet) or just install Mysql

    brew install mysql
    brew services start mysql

Connect Mysql via Sequel Pro or Table Plus appication for MacOS

    CREATE DATABASE testaspire

Run the database migrations (**Set the database connection in .env before migrating**)

    php artisan migrate

Start the local development server (if using XAMPP)

    php artisan serve

You can now access the server at http://localhost:8000 or http://127.0.0.1:8000

## API

### Running API tests locally

Using [Postman](https://www.postman.com/downloads/) for Windows and MacOS

You can use `ARC` or `Postman` app instead for Chrome.

### Register new user

`POST /api/register`

Required fields: `name`, `email`, `password`

This will return new user:

    {
        "id": 2,
        "name": "cang",
        "email": "a@gmail.com",
        "token": JWTToken,
        "created_at": "2021-06-28T07:50:26.000000Z",
        "updated_at": "2021-06-28T07:50:26.000000Z"
    }

### Authentication Header:

`Authorization: Bearer JWTToken`

### Create new loan 

`POST /api/loan`

Required fields: `amount`, `interestRate`, `repaymentFrequency`, `duration`, `arrangementFee`

This will return new loan:

    {
        "status": "Success",
        "loan": {
            "user_id": 2,
            "amount": "1000000",
            "arrangement_fee": "1",
            "interest_rate": "20",
            "repayment_frequency": "monthly",
            "duration": "12",
            "updated_at": "2021-06-28T08:07:20.000000Z",
            "created_at": "2021-06-28T08:07:20.000000Z",
            "id": 2
        }
    }

### Create new repayment 

`POST /api/repayment`

Required fields: `amount`, `loanId`

Option fields: `date`

This will return new loan:

    {
        "status": "Success",
        "repayment": {
            "loan_id": "2",
            "amount": "300000",
            "times": 1,
            "date": "2021-06-29",
            "updated_at": "2021-06-28T08:08:50.000000Z",
            "created_at": "2021-06-28T08:08:50.000000Z",
            "id": 6
        }
    }
