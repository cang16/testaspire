<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_api_register_success()
    {
        $response = $this->postJson('api/register', ['name' => 'Kang', 'email' => 'c@gmail.com', 'password' => '123456']);

        $response->assertStatus(200);
    }

    public function test_api_register_validate()
    {
        $response = $this->postJson('api/register', ['name' => 'Kang', 'email' => 'a@gmail.com']);

        $response->assertStatus(422);
    }
    public function test_api_register_exist()
    {
        $response = $this->postJson('api/register', ['name' => 'Kang', 'email' => 'c@gmail.com', 'password' => '123456']);

        $response->assertStatus(422);
    }
}
